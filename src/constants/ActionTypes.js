export const ADD_EMP = 'ADD_EMP';
export const SELECT_EMP = 'SELECT_EMP';
export const DELETE_EMP = 'DELETE_EMP';
export const EDIT_EMP = 'EDIT_EMP';

export const TOGGLE_ALL_FIELDS = 'TOGGLE_ALL_FIELDS';
export const TOGGLE_FIELD = 'TOGGLE_FIELD';

export const EDIT_FORM = 'EDIT_FORM';
export const VIEW_FORM = 'VIEW_FORM';
export const ADD_FORM = 'ADD_FORM';
