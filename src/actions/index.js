import * as types from '../constants/ActionTypes'

// employees
export const addEmpoyee = emp => ({ type: types.ADD_EMP, emp });
export const deleteEmpoyee = id => ({ type: types.DELETE_EMP, id });
export const updateEmpoyee = emp => ({ type: types.EDIT_EMP, emp });
export const selectEmployee = emp => ({ type: types.SELECT_EMP, emp });

// fields
export const toggleField = (id) => ({ type: types.TOGGLE_FIELD, id });
export const toggleAllFields = (val) => ({ type: types.TOGGLE_ALL_FIELDS, val });

// form
export const editForm = () => ({ type: types.EDIT_FORM });
export const viewForm = () => ({ type: types.VIEW_FORM });
export const addForm  = () => ({ type: types.ADD_FORM  });
