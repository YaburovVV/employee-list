import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Header from './Header'
import {Container, Row, Col} from 'react-bootstrap';
import MainSection from '../containers/MainSection'
import DetailSection from '../containers/DetailSection'
import AddSection from '../containers/AddSection'
import Settings from '../containers/Settings'
import {Provider} from "react-redux";
import {createStore} from "redux";
import reducer from "../reducers";

class App extends Component {
  constructor(props) {
    super(props);

    //Here ya go
    this.props.history.listen((location, action) => {
      console.log("on route change", location, "123",action);

    });
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
            <Header/>
          </Col>
        </Row>
        <Row>
          <Col xs lg="4">
            <MainSection/>
          </Col>
          <Col>
            <Switch>
              <Route path="/add">
                <AddSection/>
              </Route>
              <Route path="/settings">
                <Settings/>
              </Route>
              <Route path="/">
                <DetailSection/>
              </Route>
            </Switch>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
