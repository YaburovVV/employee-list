import React from 'react';
import {Control, Form, actions} from 'react-redux-form';
import {Row, Col, Button} from 'react-bootstrap'
import FormField from './FormField'
import { useHistory as hist} from "react-router-dom";


class AddSection extends React.Component {

  componentWillMount() {
    const {actions, unselectEmp} = this.props;
    actions.addForm();
    unselectEmp();
  }

  handleSubmit(emp) {
    this.props.actions.addEmpoyee(emp);
    // this.props.actions.viewForm();
    // this.props.goToMainPage()
  }

  render() {
    const {visibleFields, getList} = this.props;
    return (
      <Form
        model="initialEmpDetail"
        onSubmit={(emp) => {this.handleSubmit(emp);}}>

        {visibleFields.map(fld => {
            let model = `initialEmpDetail.${fld.name}`;
            return (
              <Row className="input-group xs-3">
                <Col lg={4} className='px-0'>
                  <label htmlFor={model}
                         className="col-form-label-lg input-group-text float-right text-right">{fld.rusName}:</label>
                </Col>
                <Col className='px-0'>
                  <FormField type={fld.type} model={model} isView={false} list={getList(fld.name)}/>
                </Col>
              </Row>
            )
          }
        )}

        <Row>
          <Col className={'d-flex justify-content-center'}>
            <Button inline type="submit" className="mx-2 my-2" variant='outline-danger'>
              Добавить
            </Button>
          </Col>
        </Row>
      </Form>
    )
  }
}

export default AddSection;
