import React from 'react';
import {Form, actions} from 'react-redux-form';
import {Row, Col, Button} from 'react-bootstrap'
import FormField from './FormField'

class DetailSection extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {actions} = this.props;
    actions.viewForm()
  }

  handleSubmit(emp) {
    this.props.actions.updateEmpoyee(emp);
    this.props.actions.viewForm();
  }

  render() {
    const {visibleFields, isViewFrom, empIsNotSelected, actions, getList} = this.props;
    if (isViewFrom && empIsNotSelected) return null;
    const {editForm} = actions;
    return (
      <Form
        model="initialEmpDetail"
        onSubmit={(emp) => this.handleSubmit(emp)}>

        {visibleFields.map(fld => {
            let model = `initialEmpDetail.${fld.name}`;
            return (
              <Row className="input-group xs-3">
                <Col lg={4} className='px-0'>
                  <label htmlFor={model}
                         className="col-form-label-lg input-group-text float-right text-right">{fld.rusName}:</label>
                </Col>
                <Col className='px-0'>
                  <FormField type={fld.type} model={model} isView={isViewFrom} list={getList(fld.name)}/>
                </Col>
              </Row>
            )
          }
        )}

        <Row>
          <Col className={'d-flex justify-content-center'}>
            {isViewFrom ?
              <>
                <button inline className="btn btn-outline-secondary mx-2 my-2 "
                        onClick={() => {editForm()}}>
                  Изменить
                </button>
              </>
              :
              <Button inline type="submit" className="mx-2 my-2" variant='outline-danger'>
                Сохранить
              </Button>
            }
          </Col>
        </Row>
      </Form>
    )
  }
}

export default DetailSection;
