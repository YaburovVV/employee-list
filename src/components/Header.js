import React from "react";
import { useHistory } from "react-router-dom";
import { Navbar, Nav } from 'react-bootstrap';

function Header({actions}) {
  let history = useHistory();
  return (
    <>
      <br/>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand onClick={() => {history.push('/')}}>Сотрудники</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link onClick={() => {history.push('/add')}}>Добавить</Nav.Link>
            <Nav.Link onClick={() => {history.push('/settings')}}>Настройки</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <br/>
    </>
  )
}

export default Header