import React from 'react';
import {
  Button,
  Table
} from 'react-bootstrap';

function MainSection({selectEmp, mainFields, empDetail, actions, isViewFrom, empMainFields, getEmpById}) {
  let {deleteEmpoyee} = actions;
  let btStyle = {variant: "outline-secondary", className: "ml-sm-2 float-right"};

  return (
    <Table bordered hover>
      <thead>
      <tr>
        {mainFields.map(fld => <th>{fld.rusName}</th>)}
      </tr>
      </thead>
      <tbody>
      {empMainFields.map((emp) =>
        <tr className={ empDetail.id === emp.id?'table-active':''} onClick={isViewFrom? () => selectEmp(getEmpById(emp.id)): null}>
          {mainFields.map(fld => <td>{emp[fld.name]}</td>)}
          <td>
            <Button {...btStyle} onClick={() => {deleteEmpoyee(emp.id)}}>Х</Button>
          </td>
        </tr>)}
      </tbody>
    </Table>)
}

export default MainSection;