import React from "react";
import {Control} from "react-redux-form";

export default function FormField({type, model, isView, list}) {
  switch (type) {
    case 'select':
      return (<Control.select model={model} className="form-control" disabled={isView}>
        <option value={null}></option>
        {list.map((item, idx) => <option value={idx}>{item}</option>)}
      </Control.select>);

    default:
      return (<Control type={type} className="form-control" model={model} id={model} disabled={isView}/>)
  }
}