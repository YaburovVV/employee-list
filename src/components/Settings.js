import React from 'react';
import {
  Button,
  Table,
  InputGroup,
  FormControl,
  Form
} from 'react-bootstrap';

function Settings({fields, actions, isAllVisibleFields}) {
  let btStyle = {variant: "outline-secondary", className: "ml-sm-2 float-right"};
  const {toggleField, toggleAllFields} = actions;
  return (
    <Form>
      <Table striped bordered hover>
        <thead>
        <tr>
          <th><Form.Check inline
                          type="checkbox"
                          checked={isAllVisibleFields}
                          label='Видимость полей'
                          onClick={(e) => {toggleAllFields(e.target.checked)}}/></th>
        </tr>
        </thead>
        <tbody>
        {fields.map((fld) =>
          <tr key={`tr-${fld.id}`}>
            <td key={`td-${fld.id}`}>
              <Form.Check checked={fld.visible}
                          inline label={fld.rusName}
                          type="checkbox"
                          id={`checkbox-${fld.id}`}
                          key={`checkbox-${fld.id}`}
                          onClick={() => {toggleField(fld.id)}}/>
            </td>
          </tr>)}
        </tbody>
      </Table>
    </Form>)
}

export default Settings;