import { SELECT_EMP } from '../constants/ActionTypes'

export const initialState = {
  id: null,
  firstName: null,
  middleName: null,
  lastName: null,
  birthday: null,
  number: null,
  post: null,
  department: null
};

export function empDetail(state = initialState, action) {
  switch (action.type) {
    case SELECT_EMP:
      return action.emp;

    default:
      return state
  }
}
