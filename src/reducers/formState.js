import {EDIT, VIEW, ADD} from "../constants/FromStates";
import {EDIT_FORM, VIEW_FORM, ADD_FORM} from "../constants/ActionTypes";

const initialState = VIEW;

export default function lists(state = initialState, action) {
  switch (action.type) {

    case EDIT_FORM:
      return EDIT;

    case VIEW_FORM:
      return VIEW;

    case ADD_FORM:
      return ADD;

    default:
      return state
  }
}
