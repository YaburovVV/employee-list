import { combineReducers } from 'redux'
import employees from './employees'
import {initialState as initialEmpDetail, empDetail} from "./empDetail";
import fields from './fields'
import lists from './lists'
import formState from './formState'
import {createForms} from 'react-redux-form'

const rootReducer = combineReducers({
  employees,
  fields,
  lists,
  empDetail,
  formState,
  ...createForms({
    initialEmpDetail,
  }),
});

export default rootReducer