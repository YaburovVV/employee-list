import {
  ADD_EMP,
  DELETE_EMP,
  EDIT_EMP
} from '../constants/ActionTypes'

const initialState = [
  {
    id: 0,
    firstName: 'Алексей',
    middleName: 'Анатольевич',
    lastName: 'Астов',
    birthday: '1993-08-12',
    number: 123,
    post: 0,
    department: 0
  },{
    id: 1,
    firstName: 'Нколай',
    middleName: '',
    lastName: 'Морозов',
    birthday: '1987-01-01',
    number: 432,
    post: 1,
    department: 1
  },{
    id: 2,
    firstName: 'Григорий',
    middleName: 'Григорьевич',
    lastName: 'Орлов',
    birthday: '1934-06-10',
    number: 653,
    post: 2,
    department: 2
  },{
    id: 3,
    firstName: 'Елизавета',
    middleName: 'Викторовна',
    lastName: 'Гордеева',
    birthday: '1993-16-09',
    number: 888,
    post: 3,
    department: 3
  }];

export default function employees(state = initialState, action) {
  switch (action.type) {
    case ADD_EMP:
      return [
        ...state,
        {
          ...action.emp,
          id: state.reduce((maxId, emp) => Math.max(emp.id, maxId), -1) + 1,
        }
      ];

    case DELETE_EMP:
      return state.filter(todo =>
        todo.id !== action.id
      );

    case EDIT_EMP:
      return state.map(emp =>
        emp.id === action.emp.id ?
          { ...emp,
            ...action.emp } :
          emp
      );

    default:
      return state
  }
}
