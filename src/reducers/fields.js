import {
  TOGGLE_ALL_FIELDS,
  TOGGLE_FIELD
} from '../constants/ActionTypes'

const initialState = [
  {
    id: 0,
    name: 'firstName',
    rusName: 'Имя',
    type: 'text',
    visible: true
  }, {
    id: 1,
    name: 'middleName',
    rusName: 'Отчетсво',
    type: 'text',
    visible: true
  }, {
    id: 2,
    name: 'lastName',
    rusName: 'Фамилия',
    type: 'text',
    visible: true
  }, {
    id: 3,
    name: 'birthday',
    rusName: 'Дата рождения',
    type: 'date',
    visible: true
  }, {
    id: 4,
    name: 'number',
    rusName: 'Табельный номер',
    type: 'number',
    visible: true
  }, {
    id: 5,
    name: 'post',
    rusName: 'Должность',
    type: 'select',
    visible: true
  }, {
    id: 6,
    name: 'department',
    rusName: 'Подразделение',
    type: 'select',
    visible: true
  }];

export default function fields(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_FIELD:
      return state.map(field => field.id === action.id ?
        {
          ...field,
          visible: !field.visible
        } : field);

    case TOGGLE_ALL_FIELDS:
      return state.map(field => ({
        ...field,
        visible: action.val
      }));

    default:
      return state
  }
}
