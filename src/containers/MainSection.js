import {connect} from 'react-redux'
import * as EmpActions from '../actions'
import {selectEmployee} from '../actions'
import {bindActionCreators} from 'redux'
import MainSection from '../components/MainSection'
import {getMainFields, getVisibleFields, isViewFrom, getEmpMainFields, getEmpById} from "../selectors";
import {actions} from "react-redux-form";

const mapStateToProps = state => ({
  employees: state.employees,
  fields: state.fields,
  visibleFields: getVisibleFields(state),
  mainFields: getMainFields(state),
  empDetail: state.empDetail,
  isViewFrom: isViewFrom(state),
  empMainFields: getEmpMainFields(state),
  getEmpById: (empId) => getEmpById(state, empId)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(EmpActions, dispatch),
  selectEmp: (emp) => {
    bindActionCreators(selectEmployee, dispatch)(emp);
    dispatch(actions.change('initialEmpDetail', emp));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainSection)
