import {connect} from 'react-redux'
import * as Actions from '../actions'
import {bindActionCreators} from 'redux'
import Settings from '../components/Settings'
import {isAllVisibleFields} from "../selectors";


const mapStateToProps = state => ({
  fields: state.fields,
  isAllVisibleFields: isAllVisibleFields(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings)

