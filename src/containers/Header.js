import { connect } from 'react-redux'
import * as EmpActions from '../actions'
import { bindActionCreators } from 'redux'
import Header from '../components/Header'

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(EmpActions, dispatch)
});

export default connect(
  null,
  mapDispatchToProps
)(Header)

