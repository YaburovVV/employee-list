import {connect} from 'react-redux'
import * as Actions from '../actions'
import {bindActionCreators} from 'redux'
import AddSection from '../components/AddSection'
import {getVisibleFields, isViewFrom, empIsNotSelected, getList, isAddFrom} from "../selectors";
import {selectEmployee} from "../actions";
import {initialState} from "../reducers/empDetail";
import {actions} from "react-redux-form";
import {useHistory as hist} from "react-router-dom";


const mapStateToProps = state => ({
  employees: state.employees,
  fields: state.fields,
  visibleFields: getVisibleFields(state),
  empDetail: state.empDetail,
  isViewFrom: isViewFrom(state),
  isAddFrom: isAddFrom(state),
  empIsNotSelected: empIsNotSelected(state),
  getList: (listName) => getList(state, listName),
  goToMainPage: () => {
    let history = hist();
    history.push('/')
  }
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch),
  unselectEmp: () => {
    bindActionCreators(selectEmployee, dispatch)(initialState);
    dispatch(actions.reset('initialEmpDetail'));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddSection)