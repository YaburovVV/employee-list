import {connect} from 'react-redux'
import * as Actions from '../actions'
import {bindActionCreators} from 'redux'
import DetailSection from '../components/DelailSection'
import {getVisibleFields, isViewFrom, empIsNotSelected, getList, isAddFrom} from "../selectors";

const mapStateToProps = state => ({
  employees: state.employees,
  fields: state.fields,
  visibleFields: getVisibleFields(state),
  empDetail: state.empDetail,
  isViewFrom: isViewFrom(state),
  isAddFrom: isAddFrom(state),
  empIsNotSelected: empIsNotSelected(state),
  getList: (listName) => getList(state, listName)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailSection)