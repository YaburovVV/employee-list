import {createSelector} from 'reselect'
import {ADD, VIEW} from "../constants/FromStates";

export const getFields = state => state.fields;
export const isViewFrom = state => state.formState === VIEW;
export const isAddFrom = state => state.formState === ADD;
export const getVisibleFields = state => isViewFrom(state) ?
  state.fields.filter(fld => fld.visible === true) :
  state.fields;

export const isAllVisibleFields = createSelector(
  [getFields, getVisibleFields],
  (fields, visibleFields) => {
    return fields.length === visibleFields.length
  }
);

export const getMainFields = state => state.fields.filter(fld => ['lastName', 'department'].includes(fld.name));
export const empIsNotSelected = state => state.empDetail.id === null;
export const getList = (state, listName) => state.lists[listName];
export const getListValue = (state, listName, listId) => getList(state, listName)[listId];
export const getEmpById = (state, empId) => state.employees.filter((emp) => emp.id === empId)[0];
export const getEmpMainFields = state => state.employees.map(emp => {
  return {
    ...emp,
    'department': getListValue(state, 'department', emp['department']),
    'post': getListValue(state, 'post', emp['post'])
  }
});

