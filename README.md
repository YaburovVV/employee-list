## Employee list

To run the project execute followings:

```
git clone git@gitlab.com:YaburovVV/employee-list.git
cd employee-list/
npm i
npm start
```

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
